/** Ex 1
 * Đầu vào: Ngày, tháng, năm
 * 
 * Các bước xử lý:
 *   B1: Kiểm tra năm nhuận, nếu nhuận thì tháng 2 có 29 ngày, không thì xét tiếp các trường hợp của năm không nhuận
 *   B2: Tháng 1,3,5,7,8,10,12 có 31 ngày. Tháng 4,6,9,11 có 30 ngày. Dựa vào thông tin này, sau đó dùng if else + else if lồng nhau để xử lý tất cả các trường hợp có thể xảy ra.
 *
 * Đầu ra: 
 *   Xuất ngày, tháng, năm của ngày tiếp theo
 *   Xuất ngày, tháng, năm của ngày trước đó
 */
function ex1NgayHomQua(){
    var ngay = document.getElementById("ex1-ngay").value *1;
    var thang = document.getElementById("ex1-thang").value *1;
    var nam = document.getElementById("ex1-nam").value *1;
    var thongBao = document.getElementById("ex1-tinh");

    if (((nam % 4 == 0 && nam % 100 != 0) || nam % 400 ==0) && (thang == 3 && ngay ==1)){
        thongBao.innerText = `👉 29/02/${nam}`;
    } else if(((nam % 4 == 0 && nam % 100 != 0) || nam % 400 ==0) && (thang == 2 && ngay ==29)){
        thongBao.innerText = `👉 28/02/${nam}`;
    } else if(thang == 3 && ngay ==1){
        thongBao.innerText = `👉 28/02/${nam}`;
    } else if((thang == 1 || thang ==2 || thang == 4 || thang == 6 || thang == 9 || thang == 11) && ngay == 1){
        if(thang ==1){
            nam--;
            thongBao.innerText = `👉 31/12/${nam}`;
        } else if(thang>10){
            thang--;
            thongBao.innerText = `👉 31/${thang}/${nam}`;
        } else{
            thang--;
            thongBao.innerText = `👉 31/0${thang}/${nam}`;
        }
    } else if((thang == 3 || thang == 5 || thang == 7 || thang == 8 || thang == 10 || thang == 12) && ngay == 1){
        if(thang>10){
            thang--;
            thongBao.innerText = `👉 30/${thang}/${nam}`;
        } else{
            thang--;
            thongBao.innerText = `👉 30/0${thang}/${nam}`;
        }
    } else if(thang > 12 || thang < 1){
        alert("Dữ liệu không hợp lệ! Vui lòng nhập lại!");
    } else if((thang == 4 || thang == 6 || thang == 9 || thang == 11) && ngay > 30){
        alert("Dữ liệu không hợp lệ! Vui lòng nhập lại!");
    } else if(thang == 2 && ngay >28){
        alert("Dữ liệu không hợp lệ! Vui lòng nhập lại!");
    } else if((thang == 1 || thang == 3 || thang == 5 || thang == 7 || thang == 8 || thang == 10 || thang == 12) && ngay >31){
        alert("Dữ liệu không hợp lệ! Vui lòng nhập lại!");
    } else{
        if(thang>=10 && ngay>10){
            ngay--;
            thongBao.innerText = `👉 ${ngay}/${thang}/${nam}`;
        } else if(thang>=10 && ngay<=10){
            ngay--;
            thongBao.innerText = `👉 0${ngay}/${thang}/${nam}`;
        } else if(ngay<=10){
            ngay--;
            thongBao.innerText = `👉 0${ngay}/0${thang}/${nam}`;
        } else{
            ngay--;
            thongBao.innerText = `👉 ${ngay}/0${thang}/${nam}`;
        }
    }
};

function ex1NgayMai(){
    var ngay = document.getElementById("ex1-ngay").value *1;
    var thang = document.getElementById("ex1-thang").value *1;
    var nam = document.getElementById("ex1-nam").value *1;
    var thongBao = document.getElementById("ex1-tinh");

    if (((nam % 4 == 0 && nam % 100 != 0) || nam % 400 ==0) && (thang == 2 && ngay ==29)){
        thongBao.innerText = `👉 01/03/${nam}`;
    } else if(((nam % 4 == 0 && nam % 100 != 0) || nam % 400 ==0) && (thang == 2 && ngay ==28)){
        thongBao.innerText = `👉 29/02/${nam}`;
    } else if(thang == 2 && ngay ==28){
        thongBao.innerText = `👉 01/03/${nam}`;
    } else if((thang == 12 || thang ==1 || thang == 3 || thang == 5 || thang == 7 || thang == 8 || thang == 10) && ngay == 31){
        if(thang ==12){
            nam++;
            thongBao.innerText = `👉 01/01/${nam}`;
        } else if(thang>10){
            thang++;
            thongBao.innerText = `👉 01/${thang}/${nam}`;
        } else{
            thang++;
            thongBao.innerText = `👉 01/0${thang}/${nam}`;
        }
    } else if((thang == 4 || thang == 6 || thang == 9 || thang == 11) && ngay == 30){
        if(thang>10){
            thang++;
            thongBao.innerText = `👉 01/${thang}/${nam}`;
        } else{
            thang++;
            thongBao.innerText = `👉 01/0${thang}/${nam}`;
        }
    } else if(thang > 12 || thang < 1){
        alert("Dữ liệu không hợp lệ! Vui lòng nhập lại!");
    } else if((thang == 4 || thang == 6 || thang == 9 || thang == 11) && ngay > 30){
        alert("Dữ liệu không hợp lệ! Vui lòng nhập lại!");
    } else if(thang == 2 && ngay >28){
        alert("Dữ liệu không hợp lệ! Vui lòng nhập lại!");
    } else if((thang == 1 || thang == 3 || thang == 5 || thang == 7 || thang == 8 || thang == 10 || thang == 12) && ngay >31){
        alert("Dữ liệu không hợp lệ! Vui lòng nhập lại!");
    } else{
        if(thang>=10 && ngay>8){
            ngay++;
            thongBao.innerText = `👉 ${ngay}/${thang}/${nam}`;
        } else if(thang>=10 && ngay<=8){
            ngay++;
            thongBao.innerText = `👉 0${ngay}/${thang}/${nam}`;
        } else if(ngay<=8){
            ngay++;
            thongBao.innerText = `👉 0${ngay}/0${thang}/${nam}`;
        } else{
            ngay++;
            thongBao.innerText = `👉 ${ngay}/0${thang}/${nam}`;
        }
    }
};

/** Ex 2
 * Đầu vào: Tháng, năm
 * 
 * Các bước xử lý:
 *   B1: Kiểm tra năm nhuận, nếu nhuận thì tháng 2 có 29 ngày, không thì tháng 2 các năm khác có 28 ngày.
 *   B2: Tháng 1,3,5,7,8,10,12 có 31 ngày. Tháng 4,6,9,11 có 30 ngày. 
 * 
 * Đầu ra: Xuất số ngày của tháng đó.
 */
function ex2TinhNgay(){
    var thang = document.getElementById("ex2-thang").value *1;
    var nam = document.getElementById("ex2-nam").value *1;
    var thongBao = document.getElementById("ex2-tinh");

    if (((nam % 4 == 0 && nam % 100 != 0) || nam % 400 ==0) && thang == 2){
        thongBao.innerText = `👉 Tháng 2 năm ${nam} có 29 ngày`;
    } else if(thang == 1 || thang ==3 || thang == 5 || thang == 7 || thang == 8 || thang == 10 || thang ==12){
        thongBao.innerText = `👉 Tháng ${thang} năm ${nam} có 31 ngày`;
    } else if(thang ==2){
        thongBao.innerText = `👉 Tháng 2 năm ${nam} có 28 ngày`;
    } else{
        thongBao.innerText = `👉 Tháng ${thang} năm ${nam} có 30 ngày`;
    }
}



/** Ex 3
 * Đầu vào: 3 số nguyên
 * 
 * Các bước xử lý:
 * B1: Tìm ra các số ở hàng đơn vị, chục, trăm
 *   donvi = n % 10;
 *   chuc = Math.floor(n/10) % 10;
 *   tram = Math.floor(n/100);
 * B2: Gọi các số ra bằng chữ tương ứng
 * B3: Xem xét các trường hợp có cách đọc khác như 101, 111, 200.
 * 
 * Đầu ra: In ra cách đọc nó
 */

function ex3DocSo(){
    var n = document.getElementById("ex3-so1").value*1;
    var ex3_thongBao = document.getElementById("ex3-doc-so");
    var donvi, chuc, tram = null;

    if(n>=100){
        donvi = n % 10;
        chuc = Math.floor(n/10) % 10;
        tram = Math.floor(n/100);
        if(tram == 1){
            demTram = "Một";
        } else if(tram == 2){
            demTram = "Hai";
        } else if(tram == 3){
            demTram = "Ba";
        } else if(tram == 4){
            demTram = "Bốn";
        } else if(tram == 5){
            demTram = "Năm";
        } else if(tram == 6){
            demTram = "Sáu";
        } else if(tram == 7){
            demTram = "Bảy";
        } else if(tram == 8){
            demTram = "Tám";
        } else{
            demTram = "Chín";
        };

        if(chuc == 2){
            demchuc = "hai";
        } else if(chuc == 3){
            demchuc = "ba";
        } else if(chuc == 4){
            demchuc = "bốn";
        } else if(chuc == 5){
            demchuc = "năm";
        } else if(chuc == 6){
            demchuc = "sáu";
        } else if(chuc == 7){
            demchuc = "bảy";
        } else if(chuc == 8){
            demchuc = "tám";
        } else{
            demchuc = "chín";
        };

        if(donvi == 1){
            demdonvi = "mốt";
        } else if(donvi == 2){
            demdonvi = "hai";
        } else if(donvi == 3){
            demdonvi = "ba";
        } else if(donvi == 4){
            demdonvi = "bốn";
        } else if(donvi == 5){
            demdonvi = "năm";
        } else if(donvi == 6){
            demdonvi = "sáu";
        } else if(donvi == 7){
            demdonvi = "bảy";
        } else if(donvi == 8){
            demdonvi = "tám";
        } else{
            demdonvi = "chín";
        };

        if(chuc == 0 && donvi == 0){
            ex3_thongBao.innerHTML=`👉 ${demTram} trăm`;
        } else if(chuc == 0){
            if(donvi == 1){
                ex3_thongBao.innerHTML=`👉 ${demTram} trăm lẻ một`;
            } else{
                ex3_thongBao.innerHTML=`👉 ${demTram} trăm lẻ ${demdonvi}`;
            }
        } else if(chuc == 1 && donvi == 1){
            ex3_thongBao.innerHTML=`👉 ${demTram} trăm mười một`;
        } else if(chuc == 1){
            ex3_thongBao.innerHTML=`👉 ${demTram} trăm mười ${demdonvi}`;
        } else{
            ex3_thongBao.innerHTML=`👉 ${demTram} trăm ${demchuc} mươi ${demdonvi}`;
        }
    } else if(n>10){
        donvi = n % 10;
        chuc = Math.floor(n/10);
        if(chuc == 2){
            demchuc = "Hai";
        } else if(chuc == 3){
            demchuc = "Ba";
        } else if(chuc == 4){
            demchuc = "Bốn";
        } else if(chuc == 5){
            demchuc = "Năm";
        } else if(chuc == 6){
            demchuc = "Sáu";
        } else if(chuc == 7){
            demchuc = "Bảy";
        } else if(chuc == 8){
            demchuc = "Tám";
        } else if(chuc == 9){
            demchuc = "Chín";
        };

        if(donvi == 1){
            demdonvi = "mốt";
        } else if(donvi == 2){
            demdonvi = "hai";
        } else if(donvi == 3){
            demdonvi = "ba";
        } else if(donvi == 4){
            demdonvi = "bốn";
        } else if(donvi == 5){
            demdonvi = "năm";
        } else if(donvi == 6){
            demdonvi = "sáu";
        } else if(donvi == 7){
            demdonvi = "bảy";
        } else if(donvi == 8){
            demdonvi = "tám";
        } else if(donvi == 9){
            demdonvi = "chín";
        };

        if(donvi == 0){
            ex3_thongBao.innerHTML=`👉 ${demchuc} mươi`;
        };
        if(chuc == 1) {
            if(donvi == 1){
                ex3_thongBao.innerHTML=`👉 Mười một`;
            } else{
                ex3_thongBao.innerHTML=`👉 Mười ${demdonvi}`;
            }
        };
        if(donvi != 0 && chuc != 1){
            ex3_thongBao.innerHTML=`👉 ${demchuc} mươi ${demdonvi}`;
        };
    } else{
        donvi = n;
        if(donvi == 1){
            demdonvi = "Một";
        } else if(donvi == 2){
            demdonvi = "Hai";
        } else if(donvi == 3){
            demdonvi = "Ba";
        } else if(donvi == 4){
            demdonvi = "Bbốn";
        } else if(donvi == 5){
            demdonvi = "Năm";
        } else if(donvi == 6){
            demdonvi = "Sáu";
        } else if(donvi == 7){
            demdonvi = "Bảy";
        } else if(donvi == 8){
            demdonvi = "Tám";
        } else if(donvi == 9){
            demdonvi = "Chín";
        } else if(donvi == 10){
            demdonvi = "Mười";
        } else{
            demdonvi = "Không";
        };
        ex3_thongBao.innerHTML=`👉 ${demdonvi}`;
    };

}

/** Ex 4
 * Đầu vào: Tên, tọa độ x, tọa độ của 3 sinh viên
 * 
 * Các bước xử lý: 
 *  B1: Tính độ dài đoạn đường của mỗi sinh viên theo công thức d = Math.sqrt((x-x1)*(x-x1) + (y-y1)*(y-y1))
 *  B2: Dùng if else để tìm ra người có d lớn nhất.
 * 
 * Đầu ra: Xuất tên sinh viên xa trường nhất.
 */

function ex4Tim(){
    var x1 = document.getElementById("ex4-x1").value *1;
    var x2 = document.getElementById("ex4-x2").value *1;
    var x3 = document.getElementById("ex4-x3").value *1;
    var x4 = document.getElementById("ex4-x4").value *1;
    var y1 = document.getElementById("ex4-y1").value *1;
    var y2 = document.getElementById("ex4-y2").value *1;
    var y3 = document.getElementById("ex4-y3").value *1;
    var y4 = document.getElementById("ex4-y4").value *1;
    var ten1 = document.getElementById("ex4-ten1").value;
    var ten2 = document.getElementById("ex4-ten2").value;
    var ten3 = document.getElementById("ex4-ten3").value;
    var thongBao = document.getElementById("ex4-tim");
    var d1,d2,d3 = 0;

    d1 = Math.sqrt((x4-x1)*(x4-x1) + (y4-y1)*(y4-y1));
    d2 = Math.sqrt((x4-x2)*(x4-x2) + (y4-y2)*(y4-y2));
    d3 = Math.sqrt((x4-x3)*(x4-x3) + (y4-y3)*(y4-y3));

    var mostD = d1;
    var mostTen= ten1;

    if (mostD == d2){
        mostTen = mostTen + " và " + ten2;
    } else if(mostD == d3){
        mostTen = mostTen + " và " + ten3;
    } else if ((d2 > mostD && d2 == d3) || (d3 > mostD && d2 == d3)){
        mostD == d2;
        mostTen = ten2 + " và " + ten3;
    } else if (d2 > mostD){
        mostD = d2;
        mostTen = ten2;
    } else if (d3 > mostD){
        mostD = d3;
        mostTen = ten3;
    } else{
        mostD = d1;
        mostTen= ten1;
    }
    thongBao.innerText =`👉 Sinh viên xa trường nhất: ${mostTen}`;
}